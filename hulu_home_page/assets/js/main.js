'use strict'
document.querySelector("header .menu_toggler")
    .addEventListener('click', () => document.querySelector("header ul").classList.toggle('active'))

const activitiesItems = [
    {
        title: "Live Sports",
        gradient: "linear-gradient(rgba(0, 0, 0, 0.8) -39.59%, rgba(0, 0, 0, 0) 117.14%)",
        description: "Catch your games at home or on the go. Stream live games from major college and pro leagues including the NCAA, NBA, NHL, NFL, and more.",
        images: ["logo_01.png","logo_02.png","logo_03.svg","logo_04.png"],
        footer: {
            text: "Live TV plan required.Regional restrictions, blackouts and additional terms apply.",
            link:"See details"
        } 
    },
    {
        title: "breaking news",
        gradient: "linear-gradient(90deg, rgba(0, 0, 0, 0.8) -57.5%, rgba(0, 0, 0, 0) 98.72%)",
        description: "Keep pace with what's going on locally and globally with trusted opinions from all the top news networks.",
        images: ["logo_11.png","logo_12.svg","logo_13.png","logo_14.svg"],
        footer: {
            text: "Live TV plan required.Regional restrictions, blackouts and additional terms apply.",
            link:"See details"
        } 
    },
    {
        title: "Biggets Events",
        gradient: "linear-gradient(90deg, rgba(0, 0, 0, 0.8) -57.5%, rgba(0, 0, 0, 0) 98.72%)",
        description: "Spectacular, can't-miss moments like the Olympics, Grammys, Oscars,Emmys, and more.",
        images: ["logo_21.png","logo_22.png","logo_23.png","logo_24.png"],
        footer: {
            text: "Live TV plan required.Regional restrictions, blackouts and additional terms apply.",
            link:"See details"
        } 
    }
]
const activitiesButtonList = document.querySelectorAll('.activity_container ul li');
const activitiesButtonSpan = document.querySelector('.activity_container ul span');
var selected = (Array.from(activitiesButtonList).filter(it => it.classList.contains('active')))[0]
const oldInfos = selected.getBoundingClientRect();
const infos = activitiesButtonSpan.getBoundingClientRect();
activitiesButtonSpan.setAttribute('style',`
    transform: scaleX(${oldInfos.width / infos.width});
`)

const IMAGE_PATH = "./assets/images/";
const title = document.querySelector('#activities .activity_item .activity_item_text h1')
const activityContainer = document.querySelector('#activities .activity_container')
const activityText = document.querySelector('#activities .activity_item .activity_item_text')
const paragraph = document.querySelector('#activities .activity_item .activity_item_text .activity_desc')
const images = document.querySelectorAll('#activities .activity_item .activity_item_text .logos_item')
const changeActivity = function (id) {
    title.innerText  = activitiesItems[id].title
    paragraph.innerText  = activitiesItems[id].description
    images.forEach((it, index) => {
        let img = it.getElementsByTagName('img')[0]
        img.src = IMAGE_PATH+activitiesItems[id].images[index]
    })
    setTimeout(() => {
        activityText.classList.remove('going')
        activityText.classList.add('comming')
    }, 150)       
}

activitiesButtonList.forEach((btn, i) => {
    btn.addEventListener('click', (e) => {
        selected.classList.remove('active')
        btn.classList.add('active')
        const newInfos = e.target.getBoundingClientRect();
        const parentInfos = e.target.offsetParent.getBoundingClientRect();

        activityText.classList.remove('comming')
        activityText.classList.add('going')
        setTimeout(() => {
            changeActivity(i)
            
            activityContainer.setAttribute('style', `  
            background-image: ${activitiesItems[i].gradient}, url(${IMAGE_PATH}activity_0${i+1}.jpg); `)
        } , 300)

        activitiesButtonSpan.setAttribute('style',`
            left: ${newInfos.left - parentInfos.left}px;
            transform: scaleX(${newInfos.width / infos.width});
        `)
        selected = e.target;
        
    });
});

// change plan button action on click
const theme = document.querySelector('#pricing .plans .change_plan');
const planContentsRows = document.querySelectorAll('#pricing .plans .row');
theme.addEventListener('click', (e) => {
    let display = theme.classList.contains('changed') ? 'none' : 'inline-block';

    planContentsRows.forEach(it => {
        const rowSpans = it.getElementsByTagName('span')
        if(theme.classList.contains('changed')) {
            it.classList.remove('display_all')}
        else {
            it.classList.add('display_all')}
        rowSpans[rowSpans.length - 1].setAttribute('style', `display:${display};`)
    })
    theme.classList.toggle('changed')
})

// footer list item click event only on mobil view
const list = document.querySelectorAll('footer .footer_top > ul > li')
list.forEach((it) => {
    it.addEventListener('click', () => it.classList.toggle('show'))
})

// helper functions
