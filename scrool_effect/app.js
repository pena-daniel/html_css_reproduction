var cr = 0;
var scrolling = 0

const createDots = function () {
    let spans = ''
    for (let index = 0; index < document.querySelectorAll("section.section").length; index++) {
        spans += `<span class='${index == 0 ? 'active ' : ''} dot_${index}'></span>`
    }
    const dots = `<div class="dots">
        ${spans}
    </div>`

    return document.createRange().createContextualFragment(dots).firstElementChild
}
const setActive = function () {
    document.querySelector(`.dots .active`).classList.remove('active')
    document.querySelector(`.dots .dot_${cr}`).classList.add('active')
}
const move = function (canscroll) {
    if (canscroll) {
        if (cr == 4) return
        cr += 1
        scrolling += 100;
        document.getElementById(
            "container"
          ).style.transform = `translateY(${-scrolling}vh)`;
    } else{
        if (cr == 0) return
        cr -= 1
        scrolling -= 100;
        document.getElementById(
            "container"
          ).style.transform = `translateY(${-scrolling}vh)`;
    }
    setActive()
}
document.addEventListener("DOMContentLoaded", () => {
    document.body.insertAdjacentElement("afterbegin", createDots())
    document.body.addEventListener('wheel', e => move(e.deltaY > 0 ? true : false))
    document.addEventListener("keydown",  (e) => {
        if(e.code == "ArrowDown") move(true)
        if(e.code == "ArrowUp") move(false)
    })
})