'use strict'

$(window).on('load', function () {
    $(".preloaded").fadeOut('slow');
});
// page scroll
$.scrollIt({
    topOffset: -50
})



const WHITE_LOGO = './assets/images/logo/white-logo.svg';
const BLACK_LOGO = './assets/images/logo/logo.svg';


$(document).ready(function () {
    var count = localStorage.getItem('counter');
    if (count == null) {
        count = "430-23-56-35";
        localStorage.setItem("counter", count);
    }
    var countArray = count.split('-');

    $('#count-days')[0].innerText = countArray[0];
    $('#count-hours')[0].innerText = countArray[1];
    $('#count-minutes')[0].innerText = countArray[2];
    $('#count-secondes')[0].innerText = countArray[3];

    var owl = $('.owl-carousel');
    // get the const value
    var days = $('#count-days')[0].innerText;
    var hours = $('#count-hours')[0].innerText;
    var minutes = $('#count-minutes')[0].innerText;
    var seconds = $('#count-secondes')[0].innerText;

    const counter = setInterval(() => {
        if (seconds == 0) {
            if (minutes == 0) {
                if (hours == 0) {
                    if (days == 0) {
                        setCount(days, hours, minutes, seconds);
                        clearInterval(counter);
                    } else {
                        $('#count-hours')[0].innerText = 24;
                        hours = 24;
                        days--;
                        $('#count-days')[0].innerText = days;
                        setCount(days, hours, minutes, seconds);
                    }
                } else {
                    $('#count-minutes')[0].innerText = 60;
                    minutes = 60;
                    hours--;
                    $('#count-hours')[0].innerText = hours;
                    setCount(days, hours, minutes, seconds);
                }
            } else {
                $('#count-secondes')[0].innerText = 60;
                seconds = 60;
                minutes--;
                $('#count-minutes')[0].innerText = minutes;
                setCount(days, hours, minutes, seconds);
            }
        } else {
            seconds--;
            $('#count-secondes')[0].innerText = seconds;
            setCount(days, hours, minutes, seconds);
        }
    }, 1000);




    function setCount(val1, val2, val3, val4) {
        const val = val1 + "-" + val2 + "-" + val3+ "-" + val4;
        localStorage.setItem('counter', val);
    }











    // Go to the next item
    $('.next-btn').click(function() {
        owl.trigger('next.owl.carousel');
    })
    // Go to the previous item
    $('.prev-btn').click(function() {
        // With optional speed parameter
        // Parameters has to be in square bracket '[]'
        owl.trigger('prev.owl.carousel', [300]);
    })


    /*--------------- testimonials carousel--------------*/
    $('.banner-carousel').owlCarousel({
        loop: true,
        autoplay: false,
        margin: 0,
        responsiveClass: true,
         slideSpeed : 300,
        paginationSpeed : 400,
        singleItem:true,
        responsive: {
            0: {
                items: 1,
                // nav:true
            },
            600: {
                items: 1,
                // nav:true
            },
            1000: {
                items: 1,
                // nav:true
            }
        }
    })

    /*--------------- brand carousel--------------*/
    $('.brand-carousel').owlCarousel({
        loop: true,
        autoplay: true,
        margin: 0,
        responsiveClass: true,
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem:false,
        responsive: {
            0: {
                items: 2,
                // nav:true
            },
            600: {
                items: 4,
                // nav:true
            },
            1000: {
                items: 6,
                // nav:true
            }
        }
    })
});