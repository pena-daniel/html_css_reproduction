'use strict'

// $(window).on('load', function () {
//     $(".preloaded").fadeOut('slow');
// });
$(window).on('load', function () {
    $(".preloaded").fadeOut('slow');
});
$(document).ready(function () {

    /*------------ Navbar Shrink --------------------*/
    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 90) {
            $('.header').addClass('navbar-shrink');
            document.querySelector('.scrool-to').style.display = 'flex';
        } else {
            $('.header').removeClass('navbar-shrink');
            document.querySelector('.scrool-to').style.display = 'none';
        }
    });

    /*--------------- testimonials carousel--------------*/
    $('.feature-carousel').owlCarousel({
        loop:true,
        autoplay: false,
        margin: 10,
        singleItem:true,
        items:1,
    })

    // page scroll
    $.scrollIt({
        topOffset: -50
    })
})
