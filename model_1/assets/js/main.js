'use strict'

// $(window).on('load', function () {
//     $(".preloaded").fadeOut('slow');
// });

const WHITE_LOGO = './assets/images/white-logo.svg';
const BLACK_LOGO = './assets/images/logo.svg';

$(window).on('load', function() {
    $(".preloaded").fadeOut('slow');
});
$(document).ready(function() {

    /*------------ Navbar Shrink --------------------*/
    $(window).on('scroll', function() {
        if ($(this).scrollTop() > 90) {
            $('.header').addClass('navbar-shrink');
            $('.header-logo')[0].attributes[1].value = BLACK_LOGO;
            document.querySelector('.scrool-to').style.display = 'flex';
        } else {
            $('.header').removeClass('navbar-shrink');
            $('.header-logo')[0].attributes[1].value = WHITE_LOGO;
            document.querySelector('.scrool-to').style.display = 'none';
        }
    });

    /*--------------- team carousel--------------*/
    $('.teams-carousel').owlCarousel({
            loop: false,
            autoplay: false,
            margin: 0,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    // nav:true
                },
                600: {
                    items: 2,
                    // nav:true
                },
                1000: {
                    items: 4,
                    // nav:true
                }
            }
        })
        /*--------------- testimonials carousel--------------*/
    $('.testimonials-carousel').owlCarousel({
        loop: true,
        autoplay: false,
        margin: 0,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                // nav:true
            },
            600: {
                items: 2,
                // nav:true
            },
            1000: {
                items: 3,
                // nav:true
            }
        }
    })

    // page scroll
    $.scrollIt({
        topOffset: -50
    })
})