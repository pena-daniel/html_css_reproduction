// import './style.css';
import {
  getDoc,
  getPdf,
  getXls,
} from './constant.js';

/**
 *
 *
 * @export
 * @class FileSkeletons
 * @extends {HTMLElement}
 *@param {ShadowRoot} root
 */
export default class FileSkeletons extends HTMLElement{
    fileList = [];
    container = null
    acceptArray = []
    icons = {
        doc: getDoc(),
        docx: getDoc(),
        pdf: getPdf(),
        xls: getXls(),
        xlsx: getXls(),
        csv: getXls()
    }
    constructor(){
        super();
        // this.root = this.attachShadow({mode: 'open'});
    }

    connectedCallback(){

        const div = this.render()
        this.insertAdjacentElement('afterend', div)

        // Listeners
        div.addEventListener('dragover', () => div.classList.add('is-hovered'))
        div.addEventListener('dragleave', () => div.classList.remove('is-hovered'))
        div.addEventListener('drop', () => div.classList.remove('is-hovered'))

        this.container = div

    }


    getAttributes(){
        return {
            label: this.getAttribute('label') || 'Drop files here or click to upload.',
            help: this.getAttribute('help') || 'a simplest files drop library',
        }
    }
    
    
    render() {
        const { label, help } = this.getAttributes()
    
        const dom = `
            <div class="img-skeleton">
                <span class="skeleton-content">
                    <h3>${label}</h3>
                    <p>${help}</p>
                </span>
                <input type="file" multiple class='skeleton-file'/>
            </div>
        `;

        const element = this.generateElement(dom);

        element.querySelector('.skeleton-file').addEventListener('change', this.onNewFiles.bind(this))
    
        return element;
        
    }

    onNewFiles(e){
        const files= this.mergeFiles(this.fileList, [...e.currentTarget.files])
        this.fileList = files[0]
        const oldFiles = files[1]
        this.updateDom(oldFiles)
    }


    mergeFiles(files1, files2){
        
        const files = [[...files1], []]
        files2.forEach(file => {
            if (files[0].find(f => f.size === file.size && f.name === file.name) === undefined) {
                files[0].push(file)
                files[1].push(file)
            }
        });

        return files
    }


    updateDom(data){
        console.log(data)
        if (this.container === null) return
        if (this.fileList.length == 0) {
            this.container.classList.remove('has-files')
            return 
        }

        this.container.classList.add('has-files')
        this.insertImg(data)
    }

    insertImg(files){
        files.forEach((file) => {
            const img = this.generateImg(file)
            // console.log(this.container)
            this.container.appendChild(img)
        })
    }
    generateImg(file){
        let source = ''
        const name = file.name.length > 9 ? `${file.name.toLowerCase().slice(0,10)+'...'}` : `${file.name.toLowerCase()}`
        const size = this.getSizeUnit(file.size);
        const type = file.name
        .split('.')
        .slice(-1)[0]
        .toLowerCase()

        // console.log(size)

        if (this.icons[type] !== undefined) {
            source = this.icons[type]
        }else{
            source = `<img  src="${URL.createObjectURL(file)}" alt="${file.name}"></img>`
        }

        const img = `
            <div class="img-skeleton__file">
                <div class="thumbnail">
                    ${source}
                </div>
                <div class="img-skeleton__file__desc">
                    <span class="name">${ name }</span>
                    <span class="size">${size}</span>
                </div>

                <div class="drop-files__delete">
                    <svg width="24" height="24" viewBox="0 0 24 24" class="drop-files__icon">
                        <path
                            d="M4 5H7V4C7 3.46957 7.21071 2.96086 7.58579 2.58579C7.96086 2.21071 8.46957 2 9 2H15C15.5304 2 16.0391 2.21071 16.4142 2.58579C16.7893 2.96086 17 3.46957 17 4V5H20C20.2652 5 20.5196 5.10536 20.7071 5.29289C20.8946 5.48043 21 5.73478 21 6C21 6.26522 20.8946 6.51957 20.7071 6.70711C20.5196 6.89464 20.2652 7 20 7H19V20C19 20.5304 18.7893 21.0391 18.4142 21.4142C18.0391 21.7893 17.5304 22 17 22H7C6.46957 22 5.96086 21.7893 5.58579 21.4142C5.21071 21.0391 5 20.5304 5 20V7H4C3.73478 7 3.48043 6.89464 3.29289 6.70711C3.10536 6.51957 3 6.26522 3 6C3 5.73478 3.10536 5.48043 3.29289 5.29289C3.48043 5.10536 3.73478 5 4 5V5ZM7 7V20H17V7H7ZM9 5H15V4H9V5ZM9 9H11V18H9V9ZM13 9H15V18H13V9Z"
                        fill="currentColor"/>
                    </svg>
                </div>
            </div>
        `;

        const imgElm = this.generateElement(img)
        imgElm.querySelector('.drop-files__delete').addEventListener('click', (e) => {
            e.stopPropagation()
            this.removeFile(file, imgElm)
        })

        return  imgElm
    }

    removeFile(file, targetParent){
        console.log(targetParent.classList)
        // console.log(e.target)
        targetParent.classList.add('leave')

        setTimeout(() => {
            targetParent.remove()
        }, 300);

        this.fileList = this.fileList.filter(f => f.size !== file.size && f.name !== file.name)

        
        setTimeout(() => {
            if (this.fileList.length == 0) {
                this.container.classList.remove('has-files')
            }
        }, 400);

    }

    getSizeUnit(size){
        let count = 0
        let unit = ''

        while (size > 1024) {
            size /= 1024;
            count++
        }

        switch (count) {
            case 0:
                unit = 'b'
                break;
            case 1:
                unit = 'ko'
                break;
            case 2:
                unit = 'mo'
                break;
            case 3:
                unit = 'go'
                break;
        }

        return  size.toFixed(1) +' '+ unit
        

        
    }

    generateElement(str){
        return document.createRange().createContextualFragment(str).firstElementChild
    }
}
