const nav = document.querySelector("header nav");
const header = document.querySelector("header");
const js_btn = document.querySelector("header .btn-toggle");
const js_cross_btn = document.querySelector("header .cross");
const breakHeader = 80



js_btn.addEventListener("click", (e) => {
  nav.classList.toggle("show");
});
js_cross_btn.addEventListener("click", (e) => {
  nav.classList.toggle("show");
});

if (window.scrollY > breakHeader && !header.classList.contains("show")) {
  header.classList.add("has_back");
}

document.addEventListener("scroll", (e) => {
  if (window.scrollY > breakHeader && !header.classList.contains("has_back")) {
    header.classList.add("has_back");
  }
  if (window.scrollY < breakHeader) {
    header.classList.remove("has_back");
  }
});
