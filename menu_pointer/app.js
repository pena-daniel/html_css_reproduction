//  fill: both
//  duration: 600
//  easing: 'cubic-bezier(.48, 1.55, .28,1)'

const nav = document.querySelector('nav')
const navItems = document.querySelectorAll('nav a')
let currentItem = document.querySelector("a[aria-selected]")
const span = document.querySelector(".separator");

navItems.forEach((value) => {
    value.addEventListener('click', onItemClisk)
})

if(currentItem) span.style.setProperty('transform', getTransform(currentItem))


/**
 * @param {Event} e
 * @returns {void}
 */
function onItemClisk(e){

    if(e.target == currentItem){
        return
    }
    currentItem.removeAttribute('aria-selected')
    currentItem = e.target
    currentItem.setAttribute('aria-selected', 'true')
    span.animate({
        transform: getTransform(e.target)
    },{
        fill: "both",
        duration: 600,
        easing: 'cubic-bezier(.48, 1.55, .28,1)'
    })
}


/**
 * @param {HTMLElement} elemnt
 * @returns {string}
 */
function getTransform(elemnt){
    const transform = {
        offsetX: elemnt.offsetLeft,
        offsetWidth: elemnt.offsetWidth/100,
    }

    return `translateX(${transform.offsetX}px) scaleX(${transform.offsetWidth})`
}